#!/bin/bash

echo "Installing Dependencies of the russel daemon ..."


nlohmann_json_repo="https://github.com/nlohmann/json.git"
eigen_repo="https://gitlab.com/libeigen/eigen.git"


if [ $# -gt 0 ]
then
	if [ $1 == "--unit-tests" ]
	then
		unit_test=true
	else
		unit_test=false
	fi
else
	unit_tests=false
fi

echo "Using Unit Tests: $unit_test ... "


# Credit. https://unix.stackexchange.com/questions/6345/how-can-i-get-distribution-name-and-version-number-in-a-simple-shell-script
if [ -f /etc/os-release ]; then
    # freedesktop.org and systemd
    . /etc/os-release
    OS=$NAME
    VER=$VERSION_ID
    LIKE=$ID_LIKE
elif type lsb_release >/dev/null 2>&1; then
    # linuxbase.org
    OS=$(lsb_release -si)
    VER=$(lsb_release -sr)
elif [ -f /etc/lsb-release ]; then
    # For some versions of Debian/Ubuntu without lsb_release command
    . /etc/lsb-release
    OS=$DISTRIB_ID
    VER=$DISTRIB_RELEASE
    LIKE="debian"
elif [ -f /etc/debian_version ]; then
    # Older Debian/Ubuntu/etc.
    OS="Debian"
    VER=$(cat /etc/debian_version)
    LIKE="debian"
elif [ -f /etc/SuSe-release ]; then
    # Older SuSE/etc.
    LIKE="debian"
    ...
elif [ -f /etc/redhat-release ]; then
    # Older Red Hat, CentOS, etc.
    ...
else
    # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
    OS=$(uname -s)
    VER=$(uname -r)
fi

echo "Found OS: $OS Version: $VER ..."

if [ "$LIKE" = "debian" ] || [ "$LIKE" = "ubuntu debian" ] 
then
	eval "apt-get install build-essential cmake git libboost-test-dev  libboost-filesystem-dev libboost-system-dev"
else
	eval "pacman -S base-devel cmake git boost"
fi

function compile(){
	mkdir build
	cd build
	eval "cmake .."
	eval "make"
	eval "make install"
	cd ..;
}


if [ ! -d "json" ]
then
	eval "git clone $nlohmann_json_repo"
fi
cd json
eval "git pull"

compile
cd ..

if [ ! -d "eigen" ]
then
	eval "git clone $eigen_repo"
fi
cd eigen
eval "git pull"

compile
cd ..


if [ $unit_test ]
then
	echo "Checking your current OS and installing boost test lib via your package manager ... "
else
	exit
fi


eval $COMMAND




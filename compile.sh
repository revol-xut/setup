#!/bin/bash

echo "Downloading Source Repositories ..."

if [ $# -gt 0 ] 
then
	if [ $1 == "development" ]
	then
		branch="development"

	else
		branch="master"
	fi
else
	branch="master"
fi

echo "Using branch $branch ... "

daemon_repo="https://revol-xut@bitbucket.org/revol-xut/russel.git"
library_repo="https://revol-xut@bitbucket.org/revol-xut/russel-interpreter.git"

function compile(){
	mkdir build;
	cd build;
	eval "cmake .."
	eval "make"
	eval "make install"
	cd ..;
}

if [ ! -d "russel" ]
then
	eval "git clone $daemon_repo"
fi

if [ ! -d "russel-interpreter" ]
then
	eval "git clone $library_repo"
fi

cd russel-interpreter
eval "git checkout $branch"
eval "git pull"

compile
cd ..

cd russel

eval "git checkout $branch"
eval "git pull"

compile
cd ..



